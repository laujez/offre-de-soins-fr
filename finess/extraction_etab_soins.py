import pandas as pd
from string import digits
import pyproj
import unidecode

def remove_accents(a):
    return unidecode.unidecode(str(a))

#récupération de la base de données finess
df_finess_france= pd.read_csv('finess-etablissements.csv', sep=';', encoding='cp1252')

df_finess_france.drop(columns=[
    'Column1','nofinessej','rslongue','complrs','compldistrib','numvoie','typvoie','voie','compvoie','lieuditbp',
    'commune','departement','telephone','telecopie','categetab','categagretab','libcategagretab',
    'siret','codeape','codemft','libmft','codesph','libsph','dateouv','dateautor','maj','numuai','datemaj','sourcecoordet']
                , axis=1
                , inplace=True)


colonne_str=['ligneacheminement','rs','libcategetab','libdepartement']

#tri etab
for colonne in colonne_str:
    df_finess_france[colonne]=df_finess_france[colonne].str.lower()

#dupliquer colonne ligneacheminement
df_finess_france['CP']=df_finess_france['ligneacheminement']

#extraction du CP
for i in range(len(df_finess_france)):
    df_finess_france.loc[i, 'CP']="".join([ele for ele in df_finess_france.loc[i, 'CP'] if ele.isdigit()])
    df_finess_france.loc[i, 'CP']=str(df_finess_france.loc[i, 'CP'])

#supprimer le code postal de la ville
table=str.maketrans('','',digits)

for i in range(len(df_finess_france)):
    df_finess_france.loc[i,'ligneacheminement']=str(df_finess_france.loc[i,'ligneacheminement']).translate(table)
    df_finess_france.loc[i, 'ligneacheminement'] = df_finess_france.loc[i, 'ligneacheminement'].lstrip(" ")
    df_finess_france.loc[i, 'ligneacheminement'] = df_finess_france.loc[i, 'ligneacheminement'].replace(' cedex','')

    #changer st en saint et ste en sainte
    df_finess_france.loc[i, 'ligneacheminement'] = df_finess_france.loc[i, 'ligneacheminement'].replace('st ', 'saint ')
    df_finess_france.loc[i, 'ligneacheminement'] = df_finess_france.loc[i, 'ligneacheminement'].replace('ste ', 'sainte ')

#enlever accent
df_finess_france['ligneacheminement']=df_finess_france['ligneacheminement'].apply(remove_accents)

#convertir en lat long
proj = pyproj.Transformer.from_crs(2154, 4326, always_xy=True)
for i in range(len(df_finess_france)):
    df_finess_france.loc[i,'coordxet'],df_finess_france.loc[i,'coordyet']= proj.transform(df_finess_france.loc[i,'coordxet'],df_finess_france.loc[i,'coordyet'])

#ajout régions et epci
#chargement des catégories d'établissements d'intéret
cat_etab_soins = pd.read_csv('cat_etab_soins_finess.csv', sep=';',encoding='cp1252')


#tri sur les établissements de soins
cat_etab_soins = cat_etab_soins['categorie_etab'].tolist()
df_finess_france = df_finess_france[df_finess_france['libcategetab'].isin(cat_etab_soins)]

#rassembler coordonnées géo
df_finess_france['coordonnees']=df_finess_france['coordxet'].astype(str)+","+df_finess_france['coordyet'].astype(str)


#ajout des régions et epci
#bases de données territoriales
EPCI_reg_dep_com=pd.read_csv(r'C:/Users/si/PycharmProjects/fonctions_extractions_finess_medecins/imagerie/webscrapping/bdd territoriale fr/epci_communes_regions_dep.csv', sep=';')

EPCI_reg_dep_com['Commune']=EPCI_reg_dep_com['Commune'].str.lower()
EPCI_reg_dep_com['Commune']=EPCI_reg_dep_com['Commune'].astype(str)
EPCI_reg_dep_com['Département']=EPCI_reg_dep_com['Département'].str.lower()
EPCI_reg_dep_com['Département']=EPCI_reg_dep_com['Département'].apply(remove_accents)
EPCI_reg_dep_com['Commune']=EPCI_reg_dep_com['Commune'].apply(remove_accents)

EPCI_reg_dep_com['CP']=EPCI_reg_dep_com['CP'].astype(str)
#rajouter un 0 au code postaux <10 000:
EPCI_reg_dep_com['CP']=EPCI_reg_dep_com['CP'].apply(lambda x: '{0:0>5}'.format(x))

#fusions des 2 dfs sur les communes
df_finess_france=df_finess_france.merge(EPCI_reg_dep_com,how='left',left_on=['libdepartement','ligneacheminement'],right_on=['Département','Commune'],copy=False)
df_finess_france=df_finess_france.drop_duplicates(subset=['nofinesset'],keep='first')
df_finess_france=df_finess_france.drop('CP_y',axis=1)
#exporter le dataframe au format csv
df_finess_france.to_csv(r'bdd_finess_etab_a_traiter.csv',sep = ';', index=False)
