# Analyse de l'offre de soins d'une zone géographique

## Description
La fonction analyse_offre_de_soins permet de produire une analyse complète de l'offre de soins d'une zone géographique. 
Le territoire d'étude peut être une région, un département, une communauté de communes ou bien une commune.  
  
**L'analyse de la zone fournit les fichiers suivants:**
* un fichier csv détaillant la densité médicale pour chaque spécialité médicale et la comparant à la densité nationale;
* un fichier csv des établissements de soins et leurs coordonées géographiques pouvant être directement importé sur my maps;
* un fichier csv des médecins généralistes et leurs coordonnées géographiques pouvant être directement importé sur my maps;
* un fichier csv des cabinets de radiologie et leur adresse pouvant directement être importé sur my maps;
* un fichier csv des laboratoires d'analyses médicales et leur coordonnées géographiques pouvant être directement importé sur my maps.
  
**Base de données utilisées :**  
  
* Les données concernant les médecins proviennent de la base de données Ameli;
* Les données concernant les établissements de soins et les laboratoires se basent sur la base de données Finess;
* La base de données sur l'imagerie a été créée à partir d'un programme de webscrapping qui a récupéré les données sur un annuaire en ligne.

## Exemple
Un dossier d'exemple d'export concernant une analyse sur le département de l'Oise est disponible (extractions données > Département_Oise)

##Utilisation de la fonction analyse_offre_de_soins
analyse_offre_de_soins(echelle,zone,nb_habitants,densite=True,medecins_generalistes=True,biologie=True,imagerie=True, etabs_de_soins=True)

Echelle= 'Région','Département','Commune','EPCI'  
Zone= 'Zone d'étude' (ex: Lille, Oise..)
 
à détailler ... 

## Améliorations en cours
* Ajouter les EPCI et les région à la base de données finess

## Améliorations futures
* Grouper certaines spécialités médicales proches entre elles;
* Produire un fichier excel pour la densité avec des couleurs (rouge si inférieure à la densité nationale / vert si supérieure)
* ...

## Auteur
Laura JEZEQUEL

## Statut du projet
En développement