import pandas as pd
import numpy as np
import unidecode
import os

def remove_accents(a):
    return unidecode.unidecode(str(a))

#creation bdd_fr_labos
#supprimer accent et majuscule comme dans la bdd_fr_imagerie
def extraction_labos_code_geo(echelle,zone):
    echelle=unidecode.unidecode(echelle)
    echelle=echelle.lower()
    echelle=echelle.replace('-',' ')

    zone=unidecode.unidecode(zone)
    zone=zone.lower()
    zone=zone.replace('-',' ')

    #chargement du csv
    path = os.getcwd()

    path1 = os.path.abspath(os.path.join(path, os.pardir))
    path1 = path1 + '\\finess\\bdd_finess_labos.csv'

    path2 = path + '\\finess\\bdd_finess_labos.csv'

    try:
        df_labos_france = pd.read_csv(r'{}'.format(path1), sep=';',encoding='cp1252')
    except:
        df_labos_france = pd.read_csv(r'{}'.format(path2), sep=';',encoding='cp1252')


    #tri sur la zone définie
    if echelle=='departement':
        echelle='libdepartement'
    if echelle=='commune':
        echelle='ligneacheminement'
    if echelle=='region':
        echelle='Région'
    if echelle=='epci':
        echelle='EPCI'

    for i in range(len(df_labos_france)):
        df_labos_france.loc[i,'EPCI'] = str(df_labos_france.loc[i,'EPCI']).replace('-', ' ')
        df_labos_france.loc[i, 'EPCI'] = str(df_labos_france.loc[i, 'EPCI']).replace("'", ' ')
        df_labos_france.loc[i,'Région'] = str(df_labos_france.loc[i,'Région']).replace('-', ' ')

    df_labos_france['EPCI'] = df_labos_france['EPCI'].apply(remove_accents)
    df_labos_france['Région'] = df_labos_france['Région'].apply(remove_accents)
    df_labos_france['EPCI'] = df_labos_france['EPCI'].str.lower()
    df_labos_france['Région'] = df_labos_france['Région'].str.lower()

    #création csv
    colonnes = ['nofinesset','rs','libdepartement','ligneacheminement','libcategetab','coordonnees','Région','EPCI']
    df_labos_zone=df_labos_france[df_labos_france[echelle]==zone]
    df_labos_zone=df_labos_zone[colonnes]
    return (df_labos_zone)
