from médecins.fonction_requete_medecins import extraction_medecins_ameli
from médecins.fonction_analyse_densite_medicale import fonction_densite
from médecins.fonction_extraction_generalistes_codes_geo import extraction_generalistes_code_geo
from biologie.fonction_extraction_labos import extraction_labos_code_geo
from imagerie.fonction_extraction_imagerie_codes_geo import extraction_imagerie_code_geo
from etablissements_de_soins.fonctions_extractions_etab_soins import extraction_etab_soins_code_geo
import os


def analyse_offre_de_soins(echelle,zone,nb_habitants,densite=True,medecins_generalistes=True,biologie=True,imagerie=True, etabs_de_soins=True):

    #médecins-----------------------------------------------------------------------------------------------------------
    # création dossier d'export si besoin
    outdir = "extractions données/{}_{}".format(echelle, zone)
    if not os.path.exists(outdir):
        os.mkdir(outdir)

    #analyse densité médicale de la zone d'étude si demandée
    if densite==True:
        df_sortie_densite=fonction_densite(echelle,zone,nb_habitants)

        #export csv
        df_sortie_densite.to_csv(str(outdir)+'/analyse_densite_medicale_{}.csv'.format(zone),sep=';', index=False)

    #extractions médecins généralistes + codes géo pour les cartographier si demandé
    if medecins_generalistes==True:
        df_sortie_generalistes=extraction_generalistes_code_geo(echelle,zone)

        #export csv
        df_sortie_generalistes.to_csv(str(outdir)+'/csv_generalistes_code_geo_{}.csv'.format(zone),sep=';', index=False)

    #biologie-----------------------------------------------------------------------------------------------------------
    if biologie==True:
        df_sortie_labos=extraction_labos_code_geo(echelle,zone)

        #export csv:
        df_sortie_labos.to_csv(str(outdir) + '/csv_labos_code_geo_{}.csv'.format(zone), sep=';',
                                      index=False)

    #imagerie-----------------------------------------------------------------------------------------------------------
    if imagerie==True:
        df_sortie_imagerie=extraction_imagerie_code_geo(echelle,zone)

        #export csv:
        df_sortie_imagerie.to_csv(str(outdir) + '/csv_imagerie_adresses_{}.csv'.format(zone), sep=';',
                                      index=False)

    #etablissements_de_soins--------------------------------------------------------------------------------------------
    if etabs_de_soins==True:
        df_sortie_etabs_soins=extraction_etab_soins_code_geo(echelle,zone)
    
        #export csv:
        df_sortie_etabs_soins.to_csv(str(outdir) + '/csv_etab_soins_code_geo_{}.csv'.format(zone), sep=';',
                                      index=False)

analyse_offre_de_soins('EPCI',"Métropole européenne de Lille",nb_habitants=1146320)