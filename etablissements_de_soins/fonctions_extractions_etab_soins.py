import pandas as pd
import numpy as np
import unidecode
import os

def remove_accents(a):
    return unidecode.unidecode(str(a))

#creation bdd_fr_imagerie labos
#supprimer accent et majuscule comme dans la bdd_fr_imagerie
def extraction_etab_soins_code_geo(echelle,zone):
    echelle=unidecode.unidecode(echelle)
    echelle=echelle.lower()
    echelle=echelle.replace('-',' ')

    zone=unidecode.unidecode(zone)
    zone=zone.lower()
    zone=zone.replace('-',' ')
    zone = zone.replace("'", ' ')

    #chargement du csv finess nettoyé
    path = os.getcwd()

    path1 = os.path.abspath(os.path.join(path, os.pardir))
    path1 = path1 + '\\finess\\bdd_finess_nettoyee_etab.csv'

    path2 = path + '\\finess\\bdd_finess_nettoyee_etab.csv'

    try:
        df_finess_etab_france = pd.read_csv(r'{}'.format(path1), sep=';', encoding='cp1252')
    except:
        df_finess_etab_france = pd.read_csv(r'{}'.format(path2), sep=';', encoding='cp1252')

    for i in range(len(df_finess_etab_france)):
        df_finess_etab_france.loc[i,'EPCI'] = str(df_finess_etab_france.loc[i,'EPCI']).replace('-', ' ')
        df_finess_etab_france.loc[i, 'EPCI'] = str(df_finess_etab_france.loc[i, 'EPCI']).replace("'", ' ')
        df_finess_etab_france.loc[i,'Région'] = str(df_finess_etab_france.loc[i,'Région']).replace('-', ' ')

    df_finess_etab_france['EPCI'] = df_finess_etab_france['EPCI'].apply(remove_accents)
    df_finess_etab_france['Région'] = df_finess_etab_france['Région'].apply(remove_accents)
    df_finess_etab_france['EPCI'] = df_finess_etab_france['EPCI'].str.lower()
    df_finess_etab_france['Région'] = df_finess_etab_france['Région'].str.lower()

    #tri sur la zone définie
    if echelle=='departement':
        echelle='libdepartement'
    if echelle=='commune':
        echelle='ligneacheminement'
    if echelle=='region':
        echelle='Région'
    if echelle=='epci':
        echelle='EPCI'

    #création csv
    colonnes = ['nofinesset','rs','libdepartement','ligneacheminement','libcategetab','coordonnees','Région','EPCI']
    df_etab_zone=df_finess_etab_france[df_finess_etab_france[echelle]==zone]
    df_etab_zone=df_etab_zone[colonnes]
    return(df_etab_zone)

