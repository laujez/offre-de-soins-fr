import pandas as pd
import numpy as np
import unidecode
import os

def remove_accents(a):
    return unidecode.unidecode(str(a))

def extraction_imagerie_code_geo(echelle,zone):
    echelle=unidecode.unidecode(echelle)
    echelle=echelle.lower()

    zone=unidecode.unidecode(zone)
    zone=zone.lower()
    zone=zone.replace('-',' ')
    zone = zone.replace('\'', ' ')

   #chargement du csv
    path = os.getcwd()

    try:
        df_imagerie = pd.read_csv(str(path)+'\\imagerie\\webscrapping\\bdd_fr_imagerie\\bdd_fr_imagerie_traitee.csv', sep=';')
    except:
        df_imagerie = pd.read_csv(str(path) + '\\webscrapping\\bdd_fr_imagerie\\bdd_fr_imagerie_traitee.csv', sep=';')

    #nettoyage
    for i in range(len(df_imagerie)):
        df_imagerie.loc[i,'EPCI'] = str(df_imagerie.loc[i,'EPCI']).replace('-', ' ')
        df_imagerie.loc[i, 'EPCI'] = str(df_imagerie.loc[i, 'EPCI']).replace("'", ' ')
        df_imagerie.loc[i,'Région'] = str(df_imagerie.loc[i,'Région']).replace('-', ' ')

    df_imagerie['EPCI'] = df_imagerie['EPCI'].apply(remove_accents)
    df_imagerie['Région'] = df_imagerie['Région'].apply(remove_accents)
    df_imagerie['EPCI'] = df_imagerie['EPCI'].str.lower()
    df_imagerie['Région'] = df_imagerie['Région'].str.lower()

    #tri sur la zone définie
    if echelle=='departement':
        echelle='Département'
    if echelle=='commune':
        echelle='Commune'
    if echelle=='epci':
        echelle='EPCI'
    if echelle=='region':
        echelle='Région'

    #création csv

    df_imagerie_zone=df_imagerie[df_imagerie[echelle]==zone]

    return(df_imagerie_zone)
