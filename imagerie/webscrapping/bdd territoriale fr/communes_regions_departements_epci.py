import pandas as pd
import unidecode
import re

"""Ce code crée une bdd_fr_imagerie nationale de l'ensemble des communes françaises associées à l'EPCI, Région, département."""

def remove_accents(a):
    return unidecode.unidecode(str(a))

#récupération de la base de données EPCI communes regions
france=pd.read_csv('georef-france-commune.csv',sep=';',encoding='cp1252')

#nettoyage de données
colonnes_france=['Nom Officiel Région','Nom Officiel Département','Nom Officiel EPCI','Nom Officiel Commune Minuscule','Code Officiel Commune']
france=france[colonnes_france]
france['Code Officiel Commune']=france['Code Officiel Commune'].astype(str)
france['Code Officiel Commune']=france['Code Officiel Commune'].str[:-2]

#enlever les caractères speciaux
elem_a_supprimer=[',','-',"'"]

france['Nom Officiel Commune Minuscule']=france['Nom Officiel Commune Minuscule'].apply(remove_accents)

for x in range(len(elem_a_supprimer)):
    france['Nom Officiel Commune Minuscule']=france['Nom Officiel Commune Minuscule'].str.replace(elem_a_supprimer[x],' ')

#merge avec une base de données nom communs des villes et non officiel pour matcher avec la base nationale d'imagerie
communes=pd.read_csv('communes-departement-region.csv', sep=';',encoding='cp1252')
print(communes)
#nettoyage de données
communes_colonnes=['code_commune_INSEE','code_postal','nom_commune_complet','nom_region','nom_departement']
communes=communes[communes_colonnes]
communes['code_commune_INSEE']=communes['code_commune_INSEE'].astype(str)
communes['code_commune_INSEE']=communes['code_commune_INSEE'].str[:-2]
communes['nom_commune_complet']=communes['nom_commune_complet'].str.lower()
communes['nom_commune_complet']=communes['nom_commune_complet'].str.replace('st ','saint ')
communes['nom_commune_complet']=communes['nom_commune_complet'].str.replace('ste ','sainte ')
communes['nom_commune_complet']=communes['nom_commune_complet'].str.replace("'",' ')
communes['nom_commune_complet']=communes['nom_commune_complet'].str.replace('-',' ')
communes['nom_commune_complet']=communes['nom_commune_complet'].apply(remove_accents)

for x in range(len(elem_a_supprimer)):
    communes['nom_commune_complet']=communes['nom_commune_complet'].str.replace(elem_a_supprimer[x],' ')
    communes['nom_departement'] = communes['nom_departement'].str.replace(elem_a_supprimer[x], ' ')

df=pd.merge(communes,france,how='left',left_on=['nom_commune_complet','nom_region'],right_on=['Nom Officiel Commune Minuscule','Nom Officiel Région'])
df_columns=['code_commune_INSEE','code_postal','nom_commune_complet','nom_region','nom_departement','Nom Officiel EPCI','Nom Officiel Commune Minuscule','Code Officiel Commune']
df=df[df_columns]
df.columns=['code_commune_INSEE','CP','Commune','Région','Département','EPCI','nom_commune_complet','Code Commune']


#rajouter un 0 au code postaux <10 000:
df['CP']=df['CP'].apply(lambda x: '{0:0>5}'.format(x))

#export base de données territoriale nationale
df.to_csv(r'epci_communes_regions_dep.csv',sep = ';', index=False,encoding='utf-8')





