import pandas as pd
import re
import unidecode

def remove_accents(a):
    return unidecode.unidecode(str(a))

"""Ce code récupère l'ensemble des bdd_fr_imagerie régionales d'imagerie et les fusionne. 
    Ensuite, il associe à chaque lieu de cabinet davantage d'information comme l'EPCI, la région, le département."""

#listes des régions
regions=['Normandie','Île-de-France','Hauts-de-France', 'Bretagne','La Réunion','Martinique','Corse','Grand Est','Bourgogne-Franche-Comté','Centre-Val de Loire','Guadeloupe','Guyane','Auvergne-Rhône-Alpes','Nouvelle-Aquitaine','Occitanie','Pays de la Loire','Provence-Alpes-Côte d\'Azur']

#création df vide
imagerie_fr=pd.DataFrame(columns = ['Nom', 'Adresse', 'Commune', 'CP', 'concat', 'catégorie d\'activité'])

#ajout csv pour chaque region dans le df vide
for region in regions:
    cabinets_region = pd.read_csv('C:/Users/si/PycharmProjects/fonctions_extractions_finess_medecins/imagerie/webscrapping/cabinets_par_régions/bdd_imagerie_{}.csv'.format(region), sep=';')
    imagerie_fr=pd.concat([imagerie_fr,cabinets_region])

#nettoyage données
imagerie_fr['Commune']=imagerie_fr['Commune'].str.lower()
imagerie_fr['Commune'] = imagerie_fr['Commune'].astype(str)
imagerie_fr['Commune']=imagerie_fr['Commune'].str.replace('st ','saint ')
imagerie_fr['Commune']=imagerie_fr['Commune'].str.replace('ste ','sainte ')
imagerie_fr['Commune']=imagerie_fr['Commune'].str.replace("'",' ')
imagerie_fr['Commune']=imagerie_fr['Commune'].apply(remove_accents)

imagerie_fr['CP'] = imagerie_fr['CP'].astype(str)

elem_a_suppr=['.0','  PIERREFITTE SUR SEINE','  PARIS 11E','  PARIS 16E','  TÉTEGHEM']

for elem in range(len(elem_a_suppr)):
    imagerie_fr['CP']=imagerie_fr['CP'].str.removesuffix(elem_a_suppr[elem])

#rajouter un 0 au code postaux <10 000:
imagerie_fr['CP']=imagerie_fr['CP'].apply(lambda x: '{0:0>5}'.format(x))

#bases de données territoriales
EPCI_reg_dep_com=pd.read_csv(r'C:/Users/si/PycharmProjects/fonctions_extractions_finess_medecins/imagerie/webscrapping/bdd territoriale fr/epci_communes_regions_dep.csv', sep=';', encoding='latin-1')

EPCI_reg_dep_com['Commune']=EPCI_reg_dep_com['Commune'].str.lower()
EPCI_reg_dep_com['Commune']=EPCI_reg_dep_com['Commune'].astype(str)
EPCI_reg_dep_com['Commune']=EPCI_reg_dep_com['Commune'].str.lower()
EPCI_reg_dep_com['Commune']=EPCI_reg_dep_com['Commune'].str.lower()
EPCI_reg_dep_com['Commune']=EPCI_reg_dep_com['Commune'].apply(remove_accents)

EPCI_reg_dep_com['CP']=EPCI_reg_dep_com['CP'].astype(str)
#rajouter un 0 au code postaux <10 000:
EPCI_reg_dep_com['CP']=EPCI_reg_dep_com['CP'].apply(lambda x: '{0:0>5}'.format(x))

#fusions des 2 dfs sur les communes
imagerie_fr=imagerie_fr.merge(EPCI_reg_dep_com,how='left',left_on=['CP','Commune'],right_on=['CP','nom_commune'],copy=False)

#création bdd_fr_imagerie imagerie fr
colonnes=['code_commune_INSEE','nom_commune','catégorie d\'activité']
imagerie_fr=imagerie_fr.drop(colonnes, axis=1)
imagerie_fr=imagerie_fr.drop_duplicates(keep='first')

imagerie_fr.to_csv(r'C:/Users/si/PycharmProjects/fonctions_extractions_finess_medecins/imagerie/webscrapping/bdd_fr_imagerie/bdd_fr_imagerie.csv', sep=';',index=False)
