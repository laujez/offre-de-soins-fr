from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from time import sleep
from selenium.webdriver.chrome.service import Service
import pandas as pd
import re
import math


def scrapping_118218(region_recherche):
    c = webdriver.ChromeOptions()
    c.add_argument("--incognito")
    #se connecter à pages jaunes
    path=Service("C:\Program Files (x86)\chromedriver.exe")
    browser= webdriver.Chrome(service=path,options=c)
    browser.implicitly_wait(0.5)
    browser.get("https://www.118712.fr/")

    sleep(1)
    #cliquer sur accepter
    accepter=browser.find_element(By.XPATH,'//*[@id="didomi-notice-agree-button"]')
    accepter.click()

    #cliquer sur la barre de recherche
    search=browser.find_element(By.XPATH,'//*[@id="search_input_mono"]')

    #écrire dans la barre de recherche
    search.send_keys("imagerie médicale {}".format(region_recherche))

    #rechercher bouton rechercher
    click_search=browser.find_element(By.XPATH,'/html/body/section/div[1]/form/div[1]/button')

    #cliquer bouton rechercher
    click_search.click()

    sleep(2)

    #reaccepter cookies
    try:
        accepter_2=browser.find_element(By.XPATH,'//*[@id="didomi-notice-agree-button"]/span')
        accepter_2.click()
    except:
        pass
    #avoir le nombre de resultats
    nb_resultats=browser.find_element(By.XPATH,'//*[@id="filtersHeader"]/div/div/div/div[1]')
    nb_resultats=nb_resultats.text
    nb_resultats=int(re.sub('\D','',nb_resultats))

    list_cabinet=[]

    compteur=20
    for i in range(1,nb_resultats):
        #compteur nombre delements avant de voir plus de résultats
        cabinet=[]
        #nom
        try:
            nom=browser.find_element(By.XPATH,'//*[@id="result_{}"]'.format(i))
            nom=nom.text
            cabinet.append(nom)
        except NoSuchElementException:
            nom=' '

        #adresse
        try:
            adresse=browser.find_element(By.XPATH,'//*[@id="item_{}"]/div[1]/div/div[2]/div[1]/h3/span[1]'.format(i))
            adresse=adresse.text
            adresse=adresse.replace(",","")
            cabinet.append(adresse)
        except NoSuchElementException:
            adresse=''
        #ville
        try:
            ville = browser.find_element(By.XPATH, '//*[@id="item_{}"]/div[1]/div/div[2]/div[1]/h3/span[3]'.format(i))
            ville = ville.text
            ville=ville.lower().capitalize()
            cabinet.append(ville)
        except NoSuchElementException:
            ville=''

        #code postal
        try:
            cp = browser.find_element(By.XPATH, '//*[@id="item_{}"]/div[1]/div/div[2]/div[1]/h3/span[2]'.format(i))
            cp = cp.text
            cabinet.append(cp)
        except NoSuchElementException:
            cp=''
        #adresse entière
        concat=adresse+' '+ville+' '+cp
        cabinet.append(concat)

        #faire défiler la page
        compteur=compteur-1
        if compteur==0:
            browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            sleep(1)
            plus_de_resultats = browser.find_element(By.XPATH, '//*[@id="more"]')
            plus_de_resultats.click()
            sleep(3)
            compteur=20

        try:
            #vérifier que c'est bien un radiologue avant de le mettre dans le csv
            categorie = browser.find_element(By.XPATH, '//*[@id="item_{}"]/div[1]/div/div[2]/h3'.format(i))
            categorie = categorie.text
            cabinet.append(categorie)
        except:
            #si pas de ctagéorie on ajoute qd même au cas où
            categorie=''
            cabinet.append(categorie)
            list_cabinet.append(cabinet)

        if categorie=='Médecins, radiologie, radiodiagnostic et imagerie médicale':
            list_cabinet.append(cabinet)

    df_cabinets=pd.DataFrame(list_cabinet, columns=['Nom','Adresse','Commune','CP','concat','catégorie d\'activité'])

    return(df_cabinets)

#régions faites='Normandie','Île-de-France','Hauts-de-France', 'Bretagne','La Réunion','Martinique','Corse','Grand Est','Bourgogne-Franche-Comté','Centre-Val de Loire','Guadeloupe','Guyane','Auvergne-Rhône-Alpes','Nouvelle-Aquitaine','Occitanie','Pays de la Loire','Provence-Alpes-Côte d\'Azur'
regions=[]

for region in regions:
    df_cabinets=scrapping_118218(region_recherche=region)
    df_cabinets.to_csv(r'C:/Users/si/PycharmProjects/fonctions_extractions_finess_medecins/imagerie/webscrapping/cabinets_par_régions/bdd_imagerie_{}.csv'.format(region), sep=';', index=False)
