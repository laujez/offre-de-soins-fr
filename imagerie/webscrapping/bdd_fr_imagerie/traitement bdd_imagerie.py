import pandas as pd
import numpy as np
import unidecode

#récupération de la base de données ameli
df_imagerie_a_traiter=pd.read_csv ('bdd_fr_imagerie_a_traiter.csv',sep=';', encoding='cp1252',dtype='unicode')

#tout mettre en minuscule
columns_string=['Commune',"Région","EPCI",'Département']

for column in columns_string:
    df_imagerie_a_traiter[column] = df_imagerie_a_traiter[column].str.lower()

#exporter le dataframe au format csv
df_imagerie_a_traiter.to_csv(r'bdd_fr_imagerie_traitee.csv',sep = ';', index=False)
