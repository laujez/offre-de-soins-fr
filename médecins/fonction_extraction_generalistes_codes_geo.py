import pandas as pd
from médecins.fonction_requete_medecins import extraction_medecins_ameli

""" fonction qui retourne pour une zone demandée(EPCI, Département,région ou ville), les médecins généralistes et les codes géo de leur lieu d'exercice"""

def extraction_generalistes_code_geo(echelle,zone):
    medecins=extraction_medecins_ameli(echelle, zone, avec_codes_ccam=False, filtre_specialites=['médecin généraliste'])
    #supprimer les médecins n'exerçant pas actuellement
    indexNames = medecins[medecins["Nature d'exercice"] == "n'exerce pas actuellement"].index
    medecins.drop(indexNames, inplace=True)

    #on garde seulement les colonnes indiquant le nom du médecin et les coordonnées géographiques du lieu d'exercice
    colonnes = ['Nom du professionnel', 'Coordonnées']
    df_generalistes_codes_geo=medecins[colonnes]
    return(df_generalistes_codes_geo)


