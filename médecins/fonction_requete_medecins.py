import pandas as pd
import os
""" créer une fonction qui retourne pour une zone demandée(EPCI, Département,région ou ville, les médecins, avec possibilité d'affiner la sélection par spécialités"""

def extraction_medecins_ameli(echelle,zone,avec_codes_ccam=True,filtre_specialites=None):
    #mis au même format que dans la bdd_fr_imagerie
    zone= zone.lower()
    echelle = echelle.lower()
    echelle = echelle.capitalize()
    if echelle=='Epci':
        echelle='EPCI'
    #chargement du csv
    path = os.getcwd()
    try:
        df_medecins = pd.read_csv(str(path)+'\\médecins\\csv\\bdd_medecins_nettoyee.csv', sep=';')
    except:
        df_medecins = pd.read_csv(str(path) + '\\csv\\bdd_medecins_nettoyee.csv', sep=';')

    #gestion des actes ccam
    if avec_codes_ccam==False:
        df_medecins.drop(columns=[
            'Base de remboursement',"Code CCAM de l'acte réalisé","Montant généralement constaté", "Borne inférieure montant",
            'Borne supérieure montant']
            , axis=1
            , inplace=True)
        df_medecins=df_medecins.drop_duplicates(subset='Nom du professionnel', keep='first')

    #filtre spécialités
    if filtre_specialites != None:
        df_medecins = df_medecins[df_medecins['Profession'].isin(filtre_specialites)]

    #filtre_zone
    if echelle=="commune":
        index = df_medecins[df_medecins['concat'].str.contains(r'{}'.format(zone))].index
        df_medecins=df_medecins.loc[index]

    df_medecins= df_medecins[df_medecins[echelle] == zone]

    return(df_medecins)







