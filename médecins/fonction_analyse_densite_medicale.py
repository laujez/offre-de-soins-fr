import pandas as pd
import sys
import os

def fonction_densite(echelle,zone,nb_habitants):
    # recuperation du module extraction_medecins_ameli
    from médecins.fonction_requete_medecins import extraction_medecins_ameli

    #récupérer ensemble des praticiens exerçant dans la zone
    medecins=extraction_medecins_ameli(echelle, zone, avec_codes_ccam=False)

    #supprimer les médecins n'exerçant pas actuellement
    indexNames = medecins[medecins["Nature d'exercice"] == "n'exerce pas actuellement"].index
    medecins.drop(indexNames, inplace=True)

    #création du dataframe de sortie

    #compter nombre de médecins par spécialités
    df_sortie = medecins['Profession'].value_counts()

    #nom des spécialités servant d'index -> colonne
    df_sortie = df_sortie.reset_index()
    df_sortie.columns = ['Spécialité', 'Nombre de médecins']

    #densité pour 100 000 habitants sur le territoire
    densite_territoire=(df_sortie['Nombre de médecins']*100000)/nb_habitants
    df_sortie=pd.concat([df_sortie,densite_territoire],axis=1)
    df_sortie.columns = ['Spécialité', 'Nombre de médecins','densité médecins/100 000 habitants']

    #densité pour 100 000 habitants en France
    path=os.getcwd()
    densite_nationale = pd.read_csv(str(path)+'\\médecins\\csv\\densite_france.csv', sep=';')

    densite_nationale=densite_nationale[['Spécialité','densite/100000habs']]

    #jointure avec notre dataframe
    df_sortie=pd.merge(df_sortie, densite_nationale, on='Spécialité')

    #indice de comparaison en %
    icp=((100*df_sortie['densité médecins/100 000 habitants'])/df_sortie['densite/100000habs'])-100
    df_sortie = pd.concat([df_sortie, icp], axis=1)

    #indice de comparaison en différence de points
    icdp=df_sortie['densité médecins/100 000 habitants']-df_sortie['densite/100000habs']
    df_sortie = pd.concat([df_sortie, icdp], axis=1)
    df_sortie.columns = ['Spécialité', 'Nombre de médecins', 'densité médecins/100 000 habitants', 'densité nationale médecins/100 000 habitants','indice de comparaison en %','indice de comparaison en différence de points']


    return(df_sortie)



