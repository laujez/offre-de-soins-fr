import pandas as pd
import os

#récupérer csv du dossier parent
path = os.getcwd()
path=os.path.abspath(os.path.join(path, os.pardir))
path=path+'\\bdd_medecins_nettoyee.csv'
medecins_fr=pd.read_csv(r'{}'.format(path), sep=';')

#supprimer actes ccam
medecins_fr.drop(columns=[
    'Base de remboursement', "Code CCAM de l'acte réalisé", "Montant généralement constaté", "Borne inférieure montant",
    'Borne supérieure montant']
    , axis=1
    , inplace=True)
medecins_fr = medecins_fr.drop_duplicates(subset='Nom du professionnel', keep='first')

#avoir le nombre de médecins en france par spé
count=medecins_fr['Profession'].value_counts()
medecins_fr_count = pd.DataFrame(count)
medecins_fr_count = medecins_fr_count.reset_index()
medecins_fr_count.columns=['Spécialité','Total']

#avoir la densité fr pour 100 000 habs
medecins_fr_count['densite/100000habs']=((medecins_fr_count['Total']*100000)/67810000)

medecins_fr_count.to_csv('densite_france.csv',index=False,sep=';')

