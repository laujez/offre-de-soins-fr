import pandas as pd
import numpy as np
import unidecode

#récupération de la base de données ameli
df_medecins=pd.read_csv ('medecins.csv',sep=';', dtype='unicode')

#nettoyage bdd_fr_imagerie

df_medecins.drop(columns=[
    'Commune','Borne Inf : 2nd intervenant', 'Borne Sup : 2nd intervenant','Montant : 2nd intervenant',
    'Montant : Imagerie', 'Borne Inf : Imagerie', 'Borne Sup : Imagerie','Libellé acte clinique','activite_principale',
    'Montant : Anesthésie', 'Borne Inf : Anesthésie',"Contrat d'accès aux soins","Mode d'exercice particulier",
    'Borne Sup : Anesthésie', 'Montant : Circulation extra corporelle','Sesam Vitale','Code Profession',"Famille de l'acte réalisé",
    'Borne Inf : Circulation extra-corporelle',"Type d'acte réalisé","Famille de l'acte technique réalisé","Acte technique réalisé",
    'Borne Sup : Circulation extra-corporelle','Code EPCI','code_insee','Code INSEE Région', 'Code INSEE Département',
    'Regroupement','Tarif hors secteur 1 / hors adhérent OPTAM/OPTAM-CO','Tarif Secteur 1 / adhérent OPTAM/OPTAM-CO']
                , axis=1
                , inplace=True)

df_medecins=df_medecins.rename(columns = {'Column 9': 'Commune'})

#tout mettre en minucules
columns=df_medecins.columns
columns_string=['Civilité',"Nature d'exercice","Convention et CAS",'Profession','Commune', 'EPCI', 'Département', 'Région',"concat"]

for column in columns_string:
    df_medecins[column] = df_medecins[column].str.lower()

#exporter le dataframe au format csv
df_medecins.to_csv(r'bdd_medecins_nettoyee1.csv',sep = ';', index=False)

